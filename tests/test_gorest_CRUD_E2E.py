from src.gorest_CRUD_E2E import GorestAPIHandler
from src.gorest_CRUD_E2E import create_random_user_data
from src.gorest_CRUD_E2E import create_new_random_email

gorestAPI = GorestAPIHandler()


def test_gorest_crud():
    # Test utworzenia użytkownika
    user_data = create_random_user_data()
    response = gorestAPI.create_new_user(user_data)
    body = response.json()
    assert "id" in body
    user_id = body['id']
    print(body)
    # Test potwierdzający, że użytkownik istnieje
    body = gorestAPI.read_user(user_id).json()
    assert body['email'] == user_data['email']
    assert body['name'] == user_data['name']
    # Test zmian w danych użytkownika
    new_email = create_new_random_email()
    body = gorestAPI.update_user(user_id, new_email).json()
    assert body['email'] == new_email['email']
    print(body)
    # Test usunięcia użytkownika
    response = gorestAPI.delete_user(user_id)
    assert response.status_code == 204
    response = gorestAPI.get_deleted_user(user_id)
    assert response.status_code == 404
    assert response.json()['message'] == 'Resource not found'